var express = require("express");
var alexa = require("alexa-app");
var promise = require("promise");

const responses = require('./json/commonResponses.json');
const configuration = require('./json/configuration.json');
const subscribers = require('./json/refdata/users.json');

const CommonMethods = require('./lib/commonMethods.js');
const MainService = require('./lib/mainservice.js');
const MailHandler = require('./lib/googlemailhandler.js');


const APP_ID = configuration.appId;
const fsmBaseURL = configuration.api_baseurl;

var IsUserRecognized = false;
var AfterConfirmation = false;
var AllParamCollected = false;

var PORT = process.env.port || 8080;
var app = express();

// ALWAYS setup the alexa app and attach it to express before anything else.
var alexaApp = new alexa.app("alexaWS");

alexaApp.express({
  expressApp: app,

  // verifies requests come from amazon alexa. Must be enabled for production.
  // You can disable this if you're running a dev environment and want to POST
  // things to test behavior. enabled by default.
  checkCert: false,

  // sets up a GET route when set to true. This is handy for testing in
  // development, but not recommended for production. disabled by default
  debug: true
});

// now POST calls to /test in express will be handled by the app.request() function

// from here on you can setup any other express routes or middlewares as normal
app.set("view engine", "ejs");


function initiateSession(request)
{

    // check if you can use session (read or write)
    if (request.hasSession())
    {
        // get the session object
        var session = request.getSession();
        var sessionDetail = session.details;
       
        var reqAppId = sessionDetail.application.applicationId;
        var alexaUserId = sessionDetail.user.userId;

        if (reqAppId != configuration.appId) {
            console.log('Invalid Alexa skill trying to call... ' + reqAppId);
            return null;
        }

        //session.set('userId',,alexaUserId);
        var currUserId = session.get('userId');

        console.log('user id identified as => ' + alexaUserId);
        console.log('current user => '+ currUserId);
   
        if (currUserId == null || currUserId == 'undefined' 
                || currUserId == '')
            {
                session.set('user','');
                session.set('user_email','');
                                                     
                session.set('appUser',{userid: "",username:"",token:"", email: ""});
                session.set('appClient','');
                session.set('appProject','');
                session.set('appPlan','');
                session.set('appBasis','');
                session.set('appValueDate','');
    
                session.set('intent_to_exec','');
                session.set('param_name','');
                session.set('info_subs','');

                var user = CommonMethods.GetUserByUserId(alexaUserId,subscribers);
                
                if (user) {
                    session.set('userId',alexaUserId);
                    session.set('user',CommonMethods.ReplaceAll(user.username,'-',' '));
                    session.set('user_email',user.email);
                    session.set('info_subs',user.username);
                    IsUserRecognized = true;
                    
                    console.log('user identified - ' + user.username + ',' + user.email + ',' + user.alexaUserId);
                }
            }
        }
}
  
function setSessionValues(request,clientname,projectname,planname,basisname,valuedt)
{
    if (request.hasSession())
    {
        // get the session object
        var session = request.getSession();
        session.set('appClient',clientname);
        session.set('appProject', projectname);
        session.set('appPlan',planname);
        session.set('appBasis', basisname);
        session.set('appValueDate',valuedt);

    }
}

function verifyValueDate(valdt) {
    
        var today = new Date();
    
        if (valdt == null || valdt == 'undefined')
        {
            valdt = CommonMethods.GetDateAsString(new Date(),'YYYY-MM-DD');
        }
        
        valdt = CommonMethods.ConvertDateYYYYMMDD_MMDDYYYY(valdt);
    
        var year = valdt.substr(6,4);
        if (eval(year) > eval(today.getFullYear()))
         {
            valdt = valdt.substr(0,2) + '-' + valdt.substr(3,2) + '-' +  today.getFullYear();
         }
    
        return valdt; 
}

function setParam(valtype,value) {

    if (value  == null || value == 'undefined' || value == '') {
        session.set('param_name',valtype);
        return true;
    }
    else {
        console.log('Requested ' + valtype + ' : ' + value);
        return false;
    }

}
    
function IsAnyNextParameterToAsk(request) {

        if (request.hasSession())
        {

            if (setParam('CLIENT',session.get('appClient')))
            {
                return true;
            }
            else {
                if (setParam('PROJECT',session.get('appProject'))) {
                    return true;
                }
                else {
                    if (setParam('PLAN',session.get('appPlan'))) {
                        return true;
                    }
                    else {
                        if (setParam('BASIS',session.get('appBasis'))) {
                            return true;
                        }
                        else {
                            if (setParam('VALUE DATE',session.get('appValueDate'))) {
                                return true;
                            }
                            else {
                                return false;
                            }
                        }
                    }
                }
            }    
        }

       return false;    
} 

function getCard(cardTitle,cardContent) {
    return {
        "type": "Simple",
        "title":cardTitle,
        "content": cardContent
    };
}

function responseToAlexa(response,responsetype,spkresponse,prompt,cardtitle,cardtext) {
       
        var cardObj = getCard(cardtitle,cardtext);
        console.log(cardObj);

        switch (responsetype.toUpperCase()) {
            case 'TELL': return response.say(spkresponse).send(); break;
            case 'ASK': return response.say(spkresponse).reprompt(prompt).shouldEndSession(false).send();break;
            case 'ASKWITHCARD': return response.card(cardtitle,cardtext).say(spkresponse).reprompt(prompt).shouldEndSession(false).send();break;
        }
}
    
function intent_GetConfirmation(request,response) {
    var session = request.getSession();
    var useremail = session.get('user_email');
    if (useremail == null || useremail == '' || useremail == 'undefined') {
        useremail = 'arun.agrawal@mercer.com';
        session.set('user_email',useremail);
    }

    responseToAlexa(response,'ASK','I will send this information to ' + useremail + ', please confirm?','','','' );
}


alexaApp.error = function(exception, request, response) {
    console.log(exception);
    response.say("Sorry, something bad happened");
};

alexaApp.pre = function(request, response, type) {
    if (request.applicationId != configuration.appId) {
      // fail ungracefully
      //throw "Invalid applicationId";
      console.log('request appId: ' + request.applicationId);
      console.log('configuration appId: ' + configuration.appId);
      return response.fail("Invalid applicationId");
    }
  };

alexaApp.post = function(request, response, type, exception) {
    console.log('sending repsonse...');
    console.log(response);

    if (exception) {
      // always turn an exception into a successful response
      console.log('exception while sending response...');
      console.log(exception);
       return response.clear().say("An error occured: " + exception).send();
    }
  };  

alexaApp.launch(function(request, response) {
    responseToAlexa(response,'ASK',responses["LaunchRequest"].ask, responses["LaunchRequest"].reprompt,"","");
});

alexaApp.intent("AMAZON.HelpIntent", {},function (request, response) {
    responseToAlexa(response,'ASK',responses["AMAZON.HelpIntent"].ask, responses["AMAZON.HelpIntent"].reprompt,"","");
});

alexaApp.intent("AMAZON.StopIntent", {},function (request, response) {
    responseToAlexa(response,'TELL',responses["ProfileFSMStopped"].tell, "","","");
});

alexaApp.intent("AMAZON.CancelIntent", {},function (request, response) {
    responseToAlexa(response,'TELL',responses["ProfileFSMStopped"].tell, "","","");
});

alexaApp.intent("SessionEndedRequest", {},function (request, response) {
    responseToAlexa(response,'TELL',responses["ProfileFSMStopped"].tell, "","","");
});

alexaApp.sessionEnded(function(request, response) {
    responseToAlexa(response,'TELL',responses["ProfileFSMStopped"].tell, "","","");
  });

alexaApp.intent("StartFSMService", {}, function(request, response) {
    responseToAlexa(response,'ASK', responses["ProfileFSMStarted"].ask, responses["ProfileFSMStarted"].reprompt,"","");
});

alexaApp.intent("StopFSMService", {}, function(request, response) {
    responseToAlexa(response,'TELL',responses["ProfileFSMStopped"].tell, "","","");
});

alexaApp.intent("GetConfirmation", {}, function(request, response) {
    intent_GetConfirmation(request, response);
});

alexaApp.intent("SetConfirmation", {}, function(request, response) {
    var cnfrmResponse = request.slot("CONSENT");
    var anotherAuthEmailId = request.slot("AUTHEMAIL");
    var session = request.getSession();
    var userId = session.get('userId');

    if (cnfrmResponse.toUpperCase() == "YES" || cnfrmResponse.toUpperCase() == "YEP" 
            || cnfrmResponse.toUpperCase() == "YAA" || cnfrmResponse.toUpperCase() == "OK" )
    {
        console.log('mail confirmed to ' + session.get('user_email'));

        if(session.get('intent_to_exec') == 'SendMeDefaultPlanFundLevel') {

                    var requestData = {
                        username: session.get('info_subs'),
                        useremail: session.get('user_email') ,
                        clientname: configuration.default_client, 
                        projectname: configuration.default_project,
                        planname: configuration.default_plan,
                        basis: configuration.default_basis,
                        reqdate: session.get('appValueDate')
                    };
           
                    var intentAction = new promise(function(resolve,reject) {
                        
                                MainService.GetFundingLevel(response,fsmBaseURL,userId,requestData,true,MailHandler, function(obj,responsetype,spkresponse,prompt,cardtitle,cardtext){
                                    var outputObj = {
                                        responsetype: responsetype,
                                        spkresponse: spkresponse,
                                        reprompt: prompt,
                                        cardtitle: cardtitle,
                                        cardtext: cardtext
                                    };
                        
                                    resolve(outputObj);
                                });
                            });
                    
                    return intentAction.then(function(data){ responseToAlexa(response,data.responsetype,data.spkresponse,data.reprompt,data.cardtitle,data.cardtext); });
                                 
            }   
            else {
                console.log('intent to exec is not matching');
            }
        }
        else {
            console.log('another email id');
            console.log(anotherAuthEmailId);
            if (anotherAuthEmailId) {
                    session.set('user_email',anotherAuthEmailId);
                    intent_GetConfirmation(request,response);
            }
            else {
                responseToAlexa(response,'ASK','okay','okay','','');
            }
        
        }
});

alexaApp.intent("GetMeDefaultPlanFundLevel", {},  function(request, response) {

    var valuedt = request.slot("VALDATE");
    var session = request.getSession();
    var userId = session.get('userId');

    console.log('input date => ' + valuedt);   
    valuedt = verifyValueDate(valuedt);
    initiateSession(request);
    session.set('appValueDate',valuedt);
   

    var requestData = {
        username: session.get('info_subs'),
        useremail: session.get('user_email') ,
        clientname: configuration.default_client, 
        projectname: configuration.default_project,
        planname: configuration.default_plan,
        basis: configuration.default_basis,
        reqdate: session.get('appValueDate')
    };

    console.log(requestData);                
  
    var intentAction = new promise(function(resolve,reject) {
        
                MainService.GetFundingLevel(response,fsmBaseURL,userId,requestData,false,MailHandler, function(obj,responsetype,spkresponse,prompt,cardtitle,cardtext){
                    var outputObj = {
                        responsetype: responsetype,
                        spkresponse: spkresponse,
                        reprompt: prompt,
                        cardtitle: cardtitle,
                        cardtext: cardtext
                    };
        
                    resolve(outputObj);
                });
            });
    
    return intentAction.then(function(data){ responseToAlexa(response,data.responsetype,data.spkresponse,data.reprompt,data.cardtitle,data.cardtext); });
                 
  });

alexaApp.intent("SendMeDefaultPlanFundLevel", {},  function(request, response) {

    var valuedt = request.slot("VALDATE");
    var session = request.getSession();
    var userId = session.get('userId');
    var sendTo = '';

    console.log('input date => ' + valuedt);   
    valuedt = verifyValueDate(valuedt);

    initiateSession(request);
    session.set('appValueDate',valuedt);
   
    sendTo = session.get('user');
    if ((!sendTo && session.get('user') !='') ||
    (sendTo && (sendTo.toUpperCase() == "ME" || sendTo.toUpperCase() == "MY EMAIL"))) {
        sendTo =   session.get('user');
    }

    //session.set('info_subs',sendTo);
    session.set('intent_to_exec','SendMeDefaultPlanFundLevel');

    intent_GetConfirmation(request, response);
  });


app.listen(PORT);
console.log("Listening on port " + PORT + ", try http://localhost:" + PORT + "/alexaWS");