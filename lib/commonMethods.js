

function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceall(str, find, replace) {
    return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function right(val,padchar,totallen) {
    var str = padchar;

    for(var i=0; i<totallen;i++)
        str += padchar;

    str += val;

    return  str.substr(str.length-totallen,str.length);
}

function isTextMatchOnly(text1,text2) {
    var result = false;
    
    if (text1 != null && text2 != null && text1 != 'undefined'
           && text2 != 'undefined') 
     {
           text1 = replaceall(text1,'(','');
           text1 = replaceall(text1,')','');
           text1 = replaceall(text1,',','');
          // text1 = replaceall(text1,'-','');
           text1 = replaceall(text1,'Inc.','Incorporation');

           text2 = replaceall(text2,'(','');
           text2 = replaceall(text2,')','');
           text2 = replaceall(text2,',','');
          // text2 = replaceall(text2,'-','');
           text2 = replaceall(text2,'Inc.','Incorporation');

           if (text1.toUpperCase() == text2.toUpperCase())
           return true;
     }

    return false;
}

module.exports = {

  SearchStringInArray: function(str, strArray) {
                            for (var j=0; j<strArray.length; j++) {
                                if (strArray[j].match(str)) return j;
                            }
                            return -1;
                        },


  ReplaceAll:          function(str, find, replace) {
                          return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
                      },

  Right:              function (val,padchar,totallen) {
                        return  right(val,padchar,totallen);
                    },
                    
  GetDateAsString:    function (dt,fmt) {
                        var date = new Date(dt);
                        var d = date.getDate();
                        var m = date.getMonth()+1;
                        var y = date.getFullYear();

                        var dateStr = y + '-' + right(m,'0',2) + '-' + right(d,'0',2);

                        if (fmt != null && fmt != 'undefined') {
                            dateStr = fmt.replace('YYYY',y);
                            dateStr = dateStr.replace('MM',right(m,'0',2));
                            dateStr = dateStr.replace('DD',right(d,'0',2));
                        }
                    
                       // console.log(dateStr);
                       return dateStr;
                    },                     

  GetEmailId:         function (username,appUsers)
                          {
                            for(var uCtr = 0;uCtr < appUsers.length; uCtr++)
                            {
                                if (username.toUpperCase() == appUsers[uCtr].username.toUpperCase())
                                {
                                   return appUsers[uCtr].email;
                                }
                            }

                            return '';
                          },     
IsTextOnlyMatch: function(text1,text2) {
                        return isTextMatchOnly(text1,text2);
                 },                       
ConvertDateYYYYMMDD_MMDDYYYY: function(srcdate) {

                    var year = srcdate.substr(0,4);
                    var mm = srcdate.substr(5,2);
                    var dt = srcdate.substr(8,2);

                    return right(mm,'0',2) + '-' + right(dt,'0',2) + '-' + year;
},                  
                        
GetValue: function (searchval,arrData,arrKey,arrVal) {
                            
                              if (arrData) {
                            
                                  for(var iCtr=0; iCtr < arrData.length; iCtr++) {
                                    var item = arrData[iCtr];

                                    if (isTextMatchOnly(item[arrKey],searchval)) {
                                   // if (item[arrKey].toUpperCase() == searchval.toUpperCase()) {
                                      return item[arrVal];
                                    }
                                  }
                              }
                            
                              return '';
                            },
                        
GetUserByUserId:        function (alexaUserId,appUsers)
                        {
                            for(var iCtr = 0;iCtr < appUsers.length; iCtr++)
                            {
                                if (alexaUserId == appUsers[iCtr].alexaUserId)
                                {
                                    return appUsers[iCtr];
                                }
                            }

                            //if user not found then return the default
                            for(var iCtr = 0;iCtr < appUsers.length; iCtr++)
                            {
                                if (appUsers[iCtr].default == "true")
                                {
                                    return appUsers[iCtr];
                                }
                            }

                            return null;

                        },

GetBasisCode:           function (basisname,appBasis)
                        {
                            for(var iCtr = 0;iCtr < appBasis.length; iCtr++)
                            {
                                if (basisname.toUpperCase() == appBasis[iCtr].basisName.toUpperCase())
                                {
                                    return appBasis[iCtr].basisCode;
                                }
                            }
                            return '';
                        },                        

GetClientByUserId:     function (appUserId,appClients)
                        {
                            for(var iCtr = 0;iCtr < appClients.length; iCtr++)
                            {
                                if (appUserId == appClients[iCtr].userId)
                                {
                                    return appClients[iCtr];
                                }
                            }
                            return null;
                        },

GetProjectByUserClientId:     function (appUserId,appClientId,appProjects)
                                {
                                    for(var iCtr = 0;iCtr < appProjects.length; iCtr++)
                                    {
                                        if (appUserId == appProjects[iCtr].userId && 
                                            appClientId == appProjects[iCtr].clientId )
                                        {
                                            return appProjects[iCtr];
                                        }
                                    }
                                    return null;
                                },

GetPlanByUserClientProjectId: function (appUserId,appClientId,appProjectId,appPlans)
                                {
                                    for(var iCtr = 0;iCtr < appPlans.length; iCtr++)
                                    {
                                        if (appUserId == appPlans[iCtr].userId && 
                                            appClientId == appPlans[iCtr].clientId &&
                                            appProjectId == appPlans[iCtr].projectId)
                                        {
                                            return appPlans[iCtr];
                                        }
                                    }
                                    return null;
                                },

}
