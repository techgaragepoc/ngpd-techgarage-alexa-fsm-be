
const FSMService = require('./fsmService.js');
const AppBasis = require('../json/refdata/basis.json');
const CommonMethods = require('./commonMethods.js');

module.exports = {

    GetFundingLevel: function(baseUrl,alexaUserId,reqData,cb)  {
        
                            var responsedata = {
                                userid: "",
                                username: "",
                                clientid: "",
                                clientname: reqData.clientname,
                                projectid: "",
                                projectname: reqData.projectname,
                                planid: "",
                                planname: reqData.planname,
                                basis: reqData.basis,
                                valuedate: reqData.reqdate,
                                fundinglevel:""
                            };
                            
                            if (reqData == null)
                            {
                                console.log('reqData is null' + reqData);
                            }
                            else 
                            {
                            
                                FSMService.Initialize(baseUrl,alexaUserId,function(user){
                                    if (user.status == 'failed') { console.log(user); cb(user,null); }
                                    else 
                                    {
                                        console.log(user);
                                        responsedata.userid = user.userid;
                                        responsedata.username = user.username;

                                        FSMService.GetClients(user.token,user.userid,function(errClient,clients) {
                                            if (errClient) {   console.log(errClient); cb(errClient,null);    }
                                            else 
                                            {
                                                console.log(clients);
        
                                                var clientId = CommonMethods.GetValue(reqData.clientname,clients,'clientname','clientid');
                                                console.log(clientId);
                                                responsedata.clientid = clientId;
                                                responsedata.clientname = reqData.clientname;
        
                                                FSMService.GetProjects(user.token,user.userid,clientId,function(err,projects) {
                                                    if (err) { console.log(err); cb(err,null); }
                                                    else 
                                                    {
                                                        console.log(projects);
        
                                                        var projId = CommonMethods.GetValue(reqData.projectname,projects,'projectname','projectid');
                                                        console.log(projId);
                                                        responsedata.projectid = projId;
                                                        responsedata.projectname = reqData.projectname;
        
                                                        FSMService.GetPlans(user.token,user.userid,clientId,projId,function(err,plans) {
                                                            if (err) { console.log(err); cb(err,null); }
                                                            else 
                                                            {
                                                                console.log(plans);
        
                                                                var planId = CommonMethods.GetValue(reqData.planname,plans,'planname','planid');
                                                                console.log(planId);
                                                                responsedata.planid = planId;
                                                                responsedata.planname = reqData.planname;
        
                                                                FSMService.GetDefaultDates(user.token,user.userid,clientId,projId,planId,function(err,defdates) {
        
                                                                    if (err) { console.log(err); cb(err,null); }
                                                                    else {
                                                                        console.log(defdates);
        
                                                                        var startdt = CommonMethods.ReplaceAll(defdates.startdate,' ','-');
                                                                        var enddt = CommonMethods.ReplaceAll(defdates.enddate,' ','-');
                                                                        var basisCode = CommonMethods.GetBasisCode(reqData.basis,AppBasis);
        
                                                                        FSMService.GetFundLevel(user.token,user.userid,clientId,projId,planId,
                                                                                startdt,enddt,basisCode,function(err,fundlvls) {
        
                                                                                    if (err) { console.log(err); cb(err,null); }
                                                                                    else {
                                                                                        console.log(fundlvls);
        
                                                                                        var fundValue = CommonMethods.GetValue(reqData.reqdate,fundlvls,'fundvaldate','fundvalue');
                                                                                        responsedata.fundinglevel = (fundValue != '')?(fundValue + '%'):' Not Found';

                                                                                        cb(null,responsedata);
                                                                                    } 
                                                                        });  //GetFundLevel
                                                                    }
        
                                                                }); //GetDefaultDates
                                                            }
        
                                                        }); //GetPlans
                                                }
        
                                                }); //GetProjects
        
                                            }
                                        });  //GetClients
                                    }
        
                                }); //Initialize
                            }
                        },
        

}