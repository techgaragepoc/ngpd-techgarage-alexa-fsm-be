
const CommonMethods = require('./commonMethods.js');
const xml2js = require('xml2js');


module.exports = {

GetUserInfo: function(userXML,callback) {
                var parseString = xml2js.parseString;
                console.log(userXML);
                parseString(userXML,function(err,result){

                    if (err) {
                        callback(err,null);
                    }
                    else {

                        if (result.ResultData && result.ResultData.userid.length > 0 &&
                            result.ResultData.dataval.length > 0 && 
                            result.ResultData.status.length > 0)
                         {
                            var user = {
                                userid: result.ResultData.userid[0],
                                token: result.ResultData.dataval[0],
                                status: result.ResultData.status[0]
                            };

                            callback(null,user);
    
                        }
                        else {
                            callback({error: 'xml is not as expected',data: result},null);
                        }

                        
                    }
               
                });
},

GetClientInfo: function(clientXML,callback) {
    var clients = [];
    var parseString = xml2js.parseString;
   // console.log(clientXML);
    parseString(clientXML,function(err,result){

        if (err) {
            callback(err,null);
        }
        else {
           // console.log(result);
            
            if (result.ArrayOfClient && result.ArrayOfClient.Client &&
                     result.ArrayOfClient.Client.length > 0)
             {
                var clientlist = result.ArrayOfClient.Client;
                for(var iCtr=0; iCtr < clientlist.length; iCtr++)  {

                    clients.push({clientid: clientlist[iCtr].id[0], clientname: clientlist[iCtr].name[0]});
                }
               
                callback(null,clients);

            }
            else {
                callback({error: 'xml is not as expected',data: result},null);
            } 

            
        }
   
    });
},

GetProjectInfo: function(projectXML,callback) {
    var projects = [];
    var parseString = xml2js.parseString;
   // console.log(clientXML);
    parseString(projectXML,function(err,result){

        if (err) {
            callback(err,null);
        }
        else {
            //console.log(result.ArrayOfProject.Project);
            
            if (result.ArrayOfProject && result.ArrayOfProject.Project &&
                     result.ArrayOfProject.Project.length > 0)
             {
                var projlist = result.ArrayOfProject.Project;
                for(var iCtr=0; iCtr < projlist.length; iCtr++)  {

                    projects.push({projectid: projlist[iCtr].id[0], projectname: projlist[iCtr].name[0]});
                }
               
                callback(null,projects);

            }
            else {
                callback({error: 'xml is not as expected',data: result},null);
            } 

            
        }
   
    });
},

GetPlanInfo: function(planXML,callback) {
    var plans = [];
    var parseString = xml2js.parseString;
   //console.log(planXML);
    parseString(planXML,function(err,result){

        if (err) {
            callback(err,null);
        }
        else {
            //console.log(result.ArrayOfPlan);
            
            if (result.ArrayOfPlan && result.ArrayOfPlan.Plan &&
                     result.ArrayOfPlan.Plan.length > 0)
             {
                var planlist = result.ArrayOfPlan.Plan;
                for(var iCtr=0; iCtr < planlist.length; iCtr++)  {

                    plans.push({planid: planlist[iCtr].id[0], planname: planlist[iCtr].name[0]});
                }
               
                callback(null,plans);

            }
            else {
                callback({error: 'xml is not as expected',data: result},null);
            } 

            
        }
   
    });
},

GetDateInfo: function(dateXML,callback){
    var defaultdates = {startdate:'', enddate: ''};
    var parseString = xml2js.parseString;
    //console.log(dateXML);
    parseString(dateXML,function(err,result){

        if (err) {
            callback(err,null);
        }
        else {
           // console.log(result.DatesInfo);
            
            if (result.DatesInfo && result.DatesInfo.EndDate_Default &&
                    result.DatesInfo.StartDate_Default)
             {

                defaultdates = {startdate: result.DatesInfo.StartDate_Default[0], enddate: result.DatesInfo.EndDate_Default[0]}
                               
                callback(null,defaultdates);

            }
            else {
                callback({error: 'xml is not as expected',data: result},null);
            } 
            
            
        }
   
    });
},

GetFundLevelInfo: function(fundlevelXML,callback) {
    var fundlevels = [];
    var parseString = xml2js.parseString;
    //console.log(fundlevelXML);
    parseString(fundlevelXML,function(err,result){

        if (err) {
            callback(err,null);
        }
        else {
           // console.log(result.ArrayOfDataPoint.DataPoint);
            
            if (result.ArrayOfDataPoint && result.ArrayOfDataPoint.DataPoint &&
                     result.ArrayOfDataPoint.DataPoint.length > 0)
             {
                var fundlvllist = result.ArrayOfDataPoint.DataPoint;
                for(var iCtr=0; iCtr < fundlvllist.length; iCtr++)  {

                    if (fundlvllist[iCtr].series[0].toUpperCase() == 'FUNDINGLEVEL')
                        fundlevels.push({fundvalue: fundlvllist[iCtr].ptVal[0], 
                                         fundvaldate: CommonMethods.ReplaceAll(fundlvllist[iCtr].valDate[0],' ','-')});
                }
               
                callback(null,fundlevels);

            }
            else {
                callback({error: 'xml is not as expected',data: result},null);
            } 

            
        }
   
    });
},

}
