const FSMService = require('./fsmService.js');
const FundLevelService = require('./fundlevelService.js');
const CommonMethods = require('./commonMethods.js');

function SendResponse(err,obj,username,responsedata,mailhandler,subscriber,callback)
{

    if (!err && responsedata)
        {

            var fundValue = (responsedata.fundinglevel != '')?responsedata.fundinglevel:' Not Found';

           // var spkResponse = "Fund level for plan " + responsedata.planname + " of project " + responsedata.projectname;
           // spkResponse +=  " of " + responsedata.basis + " on " + responsedata.valuedate + " is " + responsedata.fundinglevel;

           var spkResponse = responsedata.basis + " funding level for " + responsedata.planname;
           spkResponse +=  " on " + responsedata.valuedate + " is " + responsedata.fundinglevel;


            if (subscriber) {
                //send email
                var recipients = [];
                recipients.push(subscriber);

                var mailSubject = "Alexa Response: FSM Pension fund level on " + responsedata.valuedate;
                var mailContent = "Hi, \t\r\n \t\r\n As requested, " + spkResponse;

                mailhandler.SendMail(recipients,mailSubject,mailContent,function(err,data){
                    if (data) {
                        var spkMailResponse = data + " for FSM Pension fund level on " + responsedata.valuedate;
                        console.log('mail sent to: ' + subscriber);
                        console.log('Mail Subject: ' + mailSubject);
                        console.log('Mail Content: ' + mailContent);
                        console.log(spkMailResponse);
                        console.log(spkResponse);
                        //obj.emit(":askWithCard", spkMailResponse, "Anything else you would like to ask", "FSM Pension fund level", spkResponse);
                        callback(obj,"ASKWITHCARD", spkMailResponse, "Anything else you would like to ask", "FSM Pension fund level", spkResponse);
                    }
                    else {
                        var spkMailResponse = "Error occured while sending mail to " + subscriber + " - " + err;
                        console.log(spkMailResponse);
                        console.log(spkResponse);
                        //obj.emit(":askWithCard", spkMailResponse, "Anything else you would like to ask", "FSM Pension fund level", spkResponse);
                        callback(obj,"ASKWITHCARD", spkMailResponse, "Anything else you would like to ask", "FSM Pension fund level", spkResponse);
                    }
                });
            }
            else {
                //only speak
                console.log(spkResponse);
                //obj.emit(":askWithCard", spkResponse, "Anything else you would like to ask", "FSM Pension fund level", spkResponse);
                callback(obj,"ASKWITHCARD", spkResponse, "Anything else you would like to ask", "FSM Pension fund level", spkResponse);
            }
        }
    else
        {
            //some error occured
            console.log(responsedata);
            console.log(err);
            //obj.emit(":ask", "error occured while fetching the information, please try again <break time='3s'/>");
            callback(obj,"ASK", "error occured while fetching the information, please try again <break time='3s'/>","","","");
        }
}

module.exports = {

Initialize: function(apibaseUrl,alexaUserId,cb) {

    FSMService.Initialize(apibaseUrl,alexaUserId,function(data){
                            cb(data);
                    });

},  

GetFundingLevel: function(self,apibaseUrl,alexaUserId,requestdata,isEmailReq,mailhandler,callback)
                        {

                            console.log('request data => ' + requestdata);
                            if (requestdata == null)
                            {
                                var msg = "Sorry, seems there is some problem with request";
                                //self.emit(":ask", msg);
                                callback(self,"ASK",msg,msg,"","");
                            }
                            else {

                                var username = requestdata.username;
                                var useremail = (isEmailReq==true)?requestdata.useremail:null;

                                FundLevelService.GetFundingLevel(apibaseUrl,alexaUserId,requestdata,function(err,fundleveldata) {
                                    SendResponse(err,self,username,fundleveldata,mailhandler,useremail,callback)
                                    //cb(null,username,fundleveldata,useremail,self);
                                });
                            }
                        }

}