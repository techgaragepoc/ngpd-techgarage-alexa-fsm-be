var aws = require('aws-sdk');
var ses = new aws.SES({
   region: 'us-east-1'
});


module.exports = {


 SendMail: function(recipients,subject, content,callback) {
    
                    
                        var eParams = {
                            Destination: {
                                ToAddresses: recipients //these mail ids must be verfied email id on AWS SES
                            },
                            Message: {
                                Body: {
                                    Text: {
                                        Data: content
                                    }
                                },
                                Subject: {
                                    Data: subject
                                }
                            },
                            Source: "arun.agrawal@mercer.com" //this must be verfied email id on AWS SES
                        };

                        console.log('===SENDING EMAIL===');
                        var email = ses.sendEmail(eParams, function(err, data){
                            if(err) { callback(err.message,null); }
                            else {
                                console.log("===EMAIL SENT===");
                                callback(null,"- mail sent");
                              //  callback(null,"..mail sent to " + recipients[0]);
                              //  console.log(data);
                              //  console.log("EMAIL CODE END");
                              //  console.log('EMAIL: ', email);
                            }
                        });

    }
}