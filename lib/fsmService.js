const request = require("request");
const CommonMethods = require('./commonMethods.js');
const XMLHelper = require('./xmlHelper.js');

const appUsers = require('../json/refdata/users.json');

var isValidURL = false;
var baseurl = ''; //https://uatest-fsm.mercer.com/mfdfsmwcfService/FSMTest.svc/rest/
var fsmXMLheaders = {
  'content-type': 'application/xml'
};

var fsmJSONheaders = {
  'content-type': 'application/json'
};

function isValidValue(val) {

  var isvalid = true;

  if (val == '' || val == null || val == 'undefined')
    isvalid = false;

  return isvalid;
}

module.exports = {

    Initialize: function(apibaseurl,alexaUserId,callback) {

                      var username = '';
                      var pass = '';
                      var userfound = false;

                      for(var usrCtr = 0;usrCtr < appUsers.length; usrCtr++)
                      {

                        if (appUsers[usrCtr].alexaUserId == alexaUserId)
                          {
                            username = appUsers[usrCtr].username;
                            pass = appUsers[usrCtr].pwd;
                            userfound = true;
                            break;
                          }
                      }

                      //if userid not matched then use default
                      if (!userfound) {
                          for(var usrCtr = 0;usrCtr < appUsers.length; usrCtr++)
                          {

                            if (appUsers[usrCtr].default == "true")
                              {
                                username = appUsers[usrCtr].username;
                                pass = appUsers[usrCtr].pwd;
                                userfound = true;
                                break;
                              }
                          }
                     }
                     
                      
                      if (userfound && isValidValue(apibaseurl)) {

                          baseurl =   apibaseurl;
                          isValidURL = true;

                          var apiurl = baseurl + 'User/' + username + '/' + pass;
                          console.log(apiurl);
                          request.get({ url: apiurl, headers: fsmXMLheaders }, function(err, res, body) {

                            if (err) { 

                              callback({
                                  userid: "",
                                  username: username,
                                  token: "",
                                  status:"failed" ,
                                  error: err
                                });
                            }

                            if (body) {

                               var userId = ''; 
                               var userToken = '';
                               var result = '';

                               //console.log(body);

                                XMLHelper.GetUserInfo(body,function(err,data){
                                   if(err) {
                                      console.log(err);
                                      result = 'failed';
                                   }
                                   else {
                                    userId = data.userid;
                                    userToken = data.token;
                                    result = 'success';
                                   }

                                   callback( {
                                    userid: userId,
                                    username: username,
                                    token: userToken,
                                    status: result
                                  });
  
                                  
                                });

                            }

                          });
                      }
                    else {
                          callback( {
                            userid: "",
                            username: username,
                            token: "",
                            status:"failed",
                            error: "either user not found or invalid api base url" 
                          });
                    }  
                     
    },

    GetClients: function(token,userId,callback) {

                        if (isValidURL) {

                          if (!isValidValue(token) || !isValidValue(userId)) {
                            var msg = 'either invalid token or invalid userId';
                            console.log(msg);
                            var err = {message:msg};
                            callback(err,null);
                          }
                          else {
                              
                            var apiurl = baseurl + 'Clients/' + userId;
                            var options = {
                                            method: 'post',
                                            body: {sToken:token, sUserId:userId},
                                            json: true,
                                            url: apiurl
                                          };
                            request(options, function(err, res, body) {

                              if (err) {  
                                console.log(err);
                                callback(err,null);
                              }
                              else {
                                //console.log(body);

                                XMLHelper.GetClientInfo(body,function(err,clients){
                                  if(err) {
                                     console.log(err);
                                  }
                                  else {
                                    callback(null,clients);
                                  }
                                });
                              }
                            });
                           
                          }
                        }
                        else {
                          var msg = 'invalid api base url';
                          console.log(msg);
                          var err = {message:msg};
                          callback(err,null);
                        }

    },

    GetProjects: function(token,userId,clientId,callback) {

                              if (isValidURL) {
        
                                  if (!isValidValue(token) || !isValidValue(userId) || !isValidValue(clientId)) {
                                    var msg = 'either invalid token or invalid userId or invalid clientId';
                                    console.log(msg);
                                    var err = {message:msg};
                                    callback(err,null);
                                  }
                                  else {
                                      
                                    var apiurl = baseurl + 'Projects/' + userId + '/' + clientId;
                                    var options = {
                                      method: 'post',
                                      body: {sToken:token, sUserId:userId, sClientId: clientId},
                                      json: true,
                                      url: apiurl
                                    };

                                    request(options, function(err, res, body) {
        
                                      if (err) {  
                                        console.log(err);
                                        callback(err,null);
                                      }
                                      else {

                                        XMLHelper.GetProjectInfo(body,function(err,projects){
                                          if(err) {
                                             console.log(err);
                                          }
                                          else {
                                            callback(null,projects);
                                          }
                                        });
                                      }
                                    });
                                   
                                  }
                                }
                                else {
                                  var msg = 'invalid api base url';
                                  console.log(msg);
                                  var err = {message:msg};
                                  callback(err,null);
                                }
      
    },

    GetPlans: function(token,userId,clientId,projectId,callback) {

                                if (isValidURL) {
          
                                    if (!isValidValue(token) || !isValidValue(userId) || 
                                          !isValidValue(clientId)|| !isValidValue(projectId)) {
                                      var msg = 'either invalid token or invalid userId or invalid clientId or invalid projectid';
                                      console.log(msg);
                                      var err = {message:msg};
                                      callback(err,null);
                                    }
                                    else {
                                        
                                      var apiurl = baseurl + 'Plans/' + userId + '/' + clientId + '/' + projectId;
                                      var options = {
                                        method: 'post',
                                        body: {sToken:token, sUserId:userId, sClientId: clientId, sProjectId: projectId},
                                        json: true,
                                        url: apiurl
                                      };
  
                                      request(options, function(err, res, body) {

                                        if (err) {  
                                          console.log(err);
                                          callback(err,null);
                                        }
                                        else {

                                          XMLHelper.GetPlanInfo(body,function(err,plans){
                                            if(err) {
                                               console.log(err);
                                            }
                                            else {
                                              callback(null,plans);
                                            }
                                          });
                                        }
                                      });
                                    
                                    }
                                  }
                                  else {
                                    var msg = 'invalid api base url';
                                    console.log(msg);
                                    var err = {message:msg};
                                    callback(err,null);
                                  }
      
    },
    
    GetDefaultDates: function(token,userId,clientId,projectId,planId,callback) {
                              
                              if (isValidURL) {
        
                                  if (!isValidValue(token) || !isValidValue(userId) 
                                      || !isValidValue(clientId) || !isValidValue(projectId) || !isValidValue(planId)) {
                                    var msg = 'either invalid token or userId or clientId or projectid or planid';
                                    console.log(msg);
                                    var err = {message:msg};
                                    callback(err,null);
                                  }
                                  else {
                                      
                                    var apiurl = baseurl + 'Dates/' + userId + '/' + clientId + '/' + projectId + '/' + planId;
                                    var options = {
                                      method: 'post',
                                      body: {sToken:token, sUserId:userId, sClientId: clientId, sProjectId: projectId, sPlanId: planId},
                                      json: true,
                                      url: apiurl
                                    };

                                    request(options, function(err, res, body) {
                                   // request.get({ url: apiurl, headers: fsmheaders }, function(err, res, body) {
        
                                      if (err) {  
                                        console.log(err);
                                        callback(err,null);
                                      }
                                      else {
                                       
                                          XMLHelper.GetDateInfo(body,function(err,defaultdates){
                                            if(err) {
                                              console.log(err);
                                            }
                                            else {
                                              callback(null,defaultdates);
                                            }
                                          });

                                      }
                                    });
                                  
                                  }
                                }
                                else {
                                  var msg = 'invalid api base url';
                                  console.log(msg);
                                  var err = {message:msg};
                                  callback(err,null);
                                }
    },

    GetFundLevel: function (token,userId,clientId,projectId,planId,startdt,enddt,basis,callback) {
            
                           if (isValidURL) {
        
                                  if (!isValidValue(token) || !isValidValue(userId) 
                                      || !isValidValue(clientId) || !isValidValue(projectId) || !isValidValue(planId)) {
                                    var msg = 'either invalid token or userId or clientId or projectid or planid';
                                    console.log(msg);
                                    var err = {message:msg};
                                    callback(err,null);
                                  }
                                  else {
                                      
                                    var apiurl = baseurl + 'FundingLevel/' + userId + '/' + clientId + '/' + projectId + '/' + planId + '/12-31-2016/04-15-2017/AC/Y';
                                    var options = {
                                      method: 'post',
                                      body: {sToken:token, sUserId:userId, sClientId: clientId, sProjectId: projectId, sPlanId: planId,sStartDt: startdt, sEndDt: enddt,sBasis:basis,sHistory:'Y'},
                                      json: true,
                                      url: apiurl
                                    };

                                    request(options, function(err, res, body) {
                                   // request.get({ url: apiurl, headers: fsmheaders }, function(err, res, body) {
        
                                      if (err) {  
                                        console.log(err);
                                        callback(err,null);
                                      }
                                      else {
                                       
                                          XMLHelper.GetFundLevelInfo(body,function(err,fundlevels){
                                            if(err) {
                                              console.log(err);
                                            }
                                            else {
                                              callback(null,fundlevels);
                                            }
                                          });

                                      }
                                    });
                                  
                                  }
                                }
                                else {
                                  var msg = 'invalid api base url';
                                  console.log(msg);
                                  var err = {message:msg};
                                  callback(err,null);
                                }
                            },

}
