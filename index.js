var Alexa = require('alexa-sdk');

const Configuration = require('./json/configuration.json');
const responses = require('./json/commonResponses.json');

const AppUsers = require('./json/refdata/users.json');
const AppClients = require('./json/refdata/clients.json');
const AppProjects = require('./json/refdata/projects.json');
const AppPlans = require('./json/refdata/plans.json');
const AppBasis = require('./json/refdata/basis.json');

const CommonMethods = require('./lib/commonMethods.js');
const MainService = require('./lib/mainService.js');
const MailHandler = require('./lib/awsMailHandler.js');

const APP_ID = Configuration.appId;
const fsmBaseURL = Configuration.api_baseurl;

var IsUserRecognized = false;
var AfterConfirmation = false;
var AllParamCollected = false;

function setSessionValues(obj,clientname,projectname,planname,basisname,valuedt)
{
    obj.attributes['appClient'] = clientname;
    obj.attributes['appProject'] = projectname;
    obj.attributes['appPlan'] = planname;
    obj.attributes['appBasis'] = basisname;
    obj.attributes['appValueDate'] = valuedt;
}

function initiateSession(obj)
 {

    var currUserId = obj.attributes['userId'];
    var appUser = obj.attributes['appUser'];

    console.log('user id identified as => ' + obj.event.session.user.userId);
    console.log('current user => '+ currUserId);
    
    if (currUserId == null || currUserId == 'undefined' ||
                currUserId == '' || appUser == null || 
                appUser == 'undefined' || (appUser != null && 
                                    (appUser.token =='' || appUser.token =='undefined')))
        {

            obj.attributes['user'] = '';
            obj.attributes['user_email'] = '';

      
            obj.attributes['appUser'] = {userid: "",username:"",token:"", email: ""};
            obj.attributes['appClient'] = '';
            obj.attributes['appProject'] = '';
            obj.attributes['appPlan'] = ''
            obj.attributes['appBasis'] = '';
            obj.attributes['appValueDate'] = '';

            obj.attributes['intent_to_exec'] = '';
            obj.attributes['param_name'] = '';
            obj.attributes['info_subs'] = '';
            
            var user = CommonMethods.GetUserByUserId(obj.event.session.user.userId,AppUsers);

            if (user) {
                obj.attributes['userId'] = user.alexaUserId;
                obj.attributes['user'] = CommonMethods.ReplaceAll(user.username,'-',' ');
                obj.attributes['user_email'] = user.email;
                
                IsUserRecognized = true;

                console.log('user identified - ' + user.username + ',' + user.email + ',' + user.alexaUserId);
            }
            else {
              //  cb(null);
            }
        }
 }

 function verifyValueDate(valdt) {

    var today = new Date();

    if (valdt == null || valdt == 'undefined')
    {
        valdt = CommonMethods.GetDateAsString(new Date(),'YYYY-MM-DD');
    }
    
    valdt = CommonMethods.ConvertDateYYYYMMDD_MMDDYYYY(valdt);

    var year = valdt.substr(6,4);
    if (eval(year) > eval(today.getFullYear()))
     {
        valdt = valdt.substr(0,2) + '-' + valdt.substr(3,2) + '-' +  today.getFullYear();
     }

    return valdt; 
 }

function IsAnyNextParameterToAsk(obj) {
   
   if (obj.attributes['appClient'] == null || obj.attributes['appClient'] == 'undefined' 
            || obj.attributes['appClient'] == '') {
        obj.attributes['param_name'] = 'CLIENT';
        return true;
   }
   else {
     console.log('Requested Client: ' + obj.attributes['appClient']);
   }
        
   if (obj.attributes['appProject']  == null || obj.attributes['appProject'] == 'undefined' 
            || obj.attributes['appProject'] == '') {
    obj.attributes['param_name'] = 'PROJECT';
    return true;
   }
   else {
       console.log('Requested Project: ' + obj.attributes['appProject']);
   }

   if (obj.attributes['appPlan'] == null || obj.attributes['appPlan'] == 'undefined' 
            || obj.attributes['appPlan'] == '') {
    obj.attributes['param_name'] = 'PLAN';
    return true;
   }
   else {
       console.log('Requested Plan: ' + obj.attributes['appPlan']);
   }

   if (obj.attributes['appBasis'] == null || obj.attributes['appBasis'] == 'undefined' 
            || obj.attributes['appBasis'] == '') {
    obj.attributes['param_name'] = 'BASIS';
    return true;
   }
   else {
       console.log('Requested Basis: ' + obj.attributes['appBasis']);
   }

   if (obj.attributes['appValueDate'] == null || obj.attributes['appValueDate'] == 'undefined' 
            || obj.attributes['appValueDate'] == '') {
    obj.attributes['param_name'] = 'VALUE DATE';
    return true;
   }
   else {
       console.log('Requested Value Date: ' + obj.attributes['appValueDate']);
   }

   return false;    

} 

function responseToAlexa(obj,responsetype,spkresponse,prompt,cardtitle,cardtext) {
    
        switch (responsetype.toUpperCase()) {
            case 'TELL': obj.emit(':tell',spkresponse); break;
            case 'ASK': obj.emit(':ask',spkresponse,prompt); break;
            case 'ASKWITHCARD': obj.emit(':askWithCard',spkresponse,prompt,cardtitle,cardtext); break;
        }
        
        return;     
}


const handlers = {

    'LaunchRequest': function () {

     if (IsUserRecognized)
        {
            var welcomeMsg = 'Hi, Welcome to Mercer fund status monitor Service. how can I help you?';
            this.emit(':ask', welcomeMsg, welcomeMsg);
        }
        else {
            this.emit(':ask', responses["ProfileFSMStarted"].ask, responses["ProfileFSMStarted"].reprompt);
        }

        
    },
    'AMAZON.HelpIntent': function () {
        this.emit(':ask', responses["AMAZON.HelpIntent"].ask, responses["AMAZON.HelpIntent"].reprompt);
    },
    'AMAZON.StopIntent': function () {
        this.emit(':tell', responses["AMAZON.StopIntent"].tell);
    },
    'AMAZON.CancelIntent': function () {
        this.emit('AMAZON.StopIntent');
    },
    'SessionEndedRequest': function () {
        this.emit('AMAZON.StopIntent');
    },
    'Unhandled': function () {
        this.emit('AMAZON.HelpIntent');
    },

    
    'StartFSMService': function () {  //invoke statement: ask Mercer to start profile service

        this.emit(':ask', responses["ProfileFSMStarted"].ask, responses["ProfileFSMStarted"].reprompt);
    },
    'StopFSMService': function () {  //invoke statement: ask Mercer to stop profile service

        this.emit(':tell', responses["ProfileFSMStopped"].tell);
    },
   
    'GetConfirmation' : function() {

        if (this.attributes['user_email'] == null || this.attributes['user_email'] == 'undefined') {
            this.attributes['user_email'] = 'arun.agrawal@mercer.com';
        }

        this.emit(':ask','I will send this information to ' + this.attributes['user_email'] + ', please confirm?' );
    },

    'SetConfirmation' : function() {
        var cnfrmResponse = this.event.request.intent.slots.CONSENT.value;

        if (cnfrmResponse.toUpperCase() == "YES" || cnfrmResponse.toUpperCase() == "YEP" 
                || cnfrmResponse.toUpperCase() == "YAA" || cnfrmResponse.toUpperCase() == "OK" )
        {
            console.log('mail confirmed to => ' + this.attributes['user_email']);
            AfterConfirmation = true;
            this.emit(this.attributes['intent_to_exec']);   
        }
        else {
            console.log('mail send denied to ' + this.attributes['user_email']);
            AfterConfirmation = false;
            this.emit(':ask','ok');
          
        }
    },
    
    'GetMeDefaultPlanFundLevel': function() {
        var self = this;
        var valuedt = self.event.request.intent.slots.VALDATE.value;

        console.log('input date => ' + valuedt);   
        valuedt = verifyValueDate(valuedt);
        initiateSession(this);
        this.attributes['appValueDate'] = valuedt;
       

        var requestData = {
                            username: 'arun-agrawal',
                            useremail: 'arun.agrawal@mercer.com',
                            clientname: Configuration.default_client, 
                            projectname: Configuration.default_project,
                            planname: Configuration.default_plan,
                            basis: Configuration.default_basis,                            
                            reqdate: this.attributes['appValueDate']
                        };
        console.log(requestData);                
        MainService.GetFundingLevel(self,fsmBaseURL,self.event.session.user.userId,
            requestData, false,MailHandler,function(obj,responsetype,spkresponse,prompt,cardtitle,cardtext){
                responseToAlexa(obj,responsetype,spkresponse,prompt,cardtitle,cardtext);
        });
    },

    'SendMeDefaultPlanFundLevel': function() {

        if (!AfterConfirmation) {
                var self = this;
                var valuedt = self.event.request.intent.slots.VALDATE.value;

                console.log('input date => ' + valuedt);   
                valuedt = verifyValueDate(valuedt);
                initiateSession(this);

                this.attributes['appValueDate'] = valuedt;
                this.attributes['intent_to_exec'] = 'SendMeDefaultPlanFundLevel';
                this.emit('GetConfirmation');
    }
    else {

        var sendTo = this.attributes['info_subs'];
       
        AfterConfirmation = false;

        var requestData = {
                            username: 'arun-agrawal',
                            useremail: this.attributes['user_email'] ,
                            clientname: Configuration.default_client, 
                            projectname: Configuration.default_project,
                            planname: Configuration.default_plan,
                            basis: Configuration.default_basis,
                            reqdate: this.attributes['appValueDate']
                        };

        console.log(requestData);                
        MainService.GetFundingLevel(this,fsmBaseURL,this.event.session.user.userId,
                requestData,true,MailHandler,function(obj,responsetype,spkresponse,prompt,cardtitle,cardtext){
                    responseToAlexa(obj,responsetype,spkresponse,prompt,cardtitle,cardtext);
        });
    }
 }
};

exports.handler = function (event, context, callback) {
    var alexa = Alexa.handler(event, context);
    alexa.appId = APP_ID;
    alexa.registerHandlers(handlers);
    console.log(event.session.user.userId);
    alexa.execute();
};
